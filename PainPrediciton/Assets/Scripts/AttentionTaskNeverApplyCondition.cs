using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttentionTaskNeverApplyCondition : MonoBehaviour
{
    public List<GameObject> panelList;
    public GameObject nextSection;

    private void OnEnable()
    {
        int index = 0;
        for (int i = 0; i < panelList.Count; i++)
        {
            if (!panelList[i].activeSelf)
            {
                index++;
            }
        }

        if(index == panelList.Count)
        {
            nextSection.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}
