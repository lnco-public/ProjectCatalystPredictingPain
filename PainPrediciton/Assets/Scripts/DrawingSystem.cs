using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


/// <summary>
/// This Class handle the detection of touch, transcription of world and screen position, and 
/// translation of the drawing to values relatives to the chart dimensions
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class DrawingSystem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Camera cam;
    public Canvas UIcanva;
    public UIGridChart chart;
    public Text questionDisplayed;

    private string _category;
    private string _question;


    LineRenderer currentLineRenderer;
    RectTransform image;
    Vector2 lastPos;
    string time;
    List<string> timeStamps = new List<string>();

    private void Awake()
    {
        currentLineRenderer = GetComponent<LineRenderer>();
    }

    void OnEnable()
    {
        UIcanva.gameObject.SetActive(false);

        //if (currentLineRenderer.enabled)
        //    currentLineRenderer.enabled = false;

    }

    private void Start()
    {
        //GameObject brushInstance = Instantiate(brush, transform);
        currentLineRenderer.enabled = false;
        image = transform.parent.GetComponent<RectTransform>();
    }

    private void Update()
    {
        
    }

    public void SetCategory(Text c) { { _category = c.text; } }

    public void SetQuestion(Text q)
    {
        {
            _question = q.text;
            questionDisplayed.text = _question;
        }
    }

    public void OnBeginDrag(PointerEventData data)
    {
        if (!currentLineRenderer.enabled)
            currentLineRenderer.enabled = true;

        Vector3 posConv = cam.ScreenToWorldPoint(new Vector3(data.pressPosition.x, data.pressPosition.y, 0));
        Vector2 pos = new Vector2(posConv.x, posConv.y);

        currentLineRenderer.positionCount = 1;
        currentLineRenderer.SetPosition(0, pos);
        lastPos = pos;
        if (timeStamps.Count != 0)
        {
            timeStamps.Clear();
        }
        timeStamps.Add(GameManager.Instance.GetTime());
    }

    public void OnDrag(PointerEventData data)
    {
        Vector3 posConv = cam.ScreenToWorldPoint(new Vector3(data.position.x, data.position.y, 0));
        Vector2 pos = new Vector2(posConv.x, posConv.y);
        //Debug.Log(pos);
        if (lastPos != pos && lastPos.y >= pos.y && RectTransformUtility.RectangleContainsScreenPoint(image, data.position, cam))
        {
            currentLineRenderer.positionCount++;
            int positionIndex = currentLineRenderer.positionCount - 1;
            currentLineRenderer.SetPosition(positionIndex, pos);
            lastPos = pos;
            //timeStamps.Add(DateTime.Now.Ticks);
            timeStamps.Add(GameManager.Instance.GetTime());
        }
    }

    public void OnEndDrag(PointerEventData data)
    {

    }

    public void SaveAndErase()
    {

        //string results =
        //    DateTime.Now.Ticks + "," +
        //    _category + "," +
        //    "Submit rating" + "," +
        //    _question +
        //    Environment.NewLine;

        GameManager.Instance.SetData(
            GameManager.Instance.GetTime(),
            _category,
            "Submit rating",
            _question,
            "",
            "",
            "",
            ""
            );
        if(currentLineRenderer.positionCount > 2)
        {
            for (int i = 0; i < currentLineRenderer.positionCount; i++)
            {
                float yValue = (Camera.main.WorldToScreenPoint(currentLineRenderer.GetPosition(i)).x - chart.origin.x) * chart.gridSize.x / (chart.chartMax.x - chart.origin.x);
                yValue = Mathf.Clamp(yValue, 0, chart.gridSize.x);
                float xValue = (Camera.main.WorldToScreenPoint(currentLineRenderer.GetPosition(i)).y - chart.origin.y) * chart.gridSize.y / (chart.chartMax.y - chart.origin.y);
                xValue = Mathf.Clamp(xValue, 0, chart.gridSize.y);
                //results +=
                //    timeStamps[i] + "," +
                //    "," +
                //    "," +
                //    "," +    
                //    i.ToString() + "," +
                //    xValue.ToString() + "," +
                //    yValue.ToString() + "," +
                //    Environment.NewLine;

                GameManager.Instance.SetData(
                    timeStamps[i],
                    "",
                    "Saved drawing data",
                    "",
                    "",
                    i.ToString("N"),
                    xValue.ToString("F6"),
                    yValue.ToString("F6")
                    );

            }
        }
        else
        {
            GameManager.Instance.SetData(
            GameManager.Instance.GetTime(),
            _category,
            "Submit rating with no answer",
            _question,
            "",
            "",
            "",
            ""
            );
        }

        //File.AppendAllText("Results" + ".txt", results);

        //          File.AppendAllText("StandardSaccade_" + UserID + ".txt", value);

        UIcanva.gameObject.SetActive(false);

        //erase the current drawing
        //currentLineRenderer.positionCount = 1;
    }

    public void EraseGraph()
    {
        //erase the current drawing
        if (currentLineRenderer != null && currentLineRenderer.positionCount > 1)
        {
            currentLineRenderer.positionCount = 1;
        }
    }
}

