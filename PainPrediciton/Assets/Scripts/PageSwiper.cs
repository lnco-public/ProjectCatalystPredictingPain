using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// This class handle the swipe between different panel in a UI canva
/// </summary>
public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;

    public bool resetOnEnable = false;

    public float percentThreshold = 0.2f;
    public float easing = 0.5f;
    public int totalPages = 1;
    [SerializeField]
    private int currentPage = 1;

    // Start is called before the first frame update
    void Start()
    {
        panelLocation = transform.position;
    }

    void OnEnable()
    {
        if (resetOnEnable)
        {
            GoToPanel(1);
        }
    }

    public void NextPage()
    {
        Vector3 newLocation = panelLocation;
        if (currentPage < totalPages)
        {
            currentPage++;
            newLocation += new Vector3(-Screen.width, 0, 0);


            GameManager.Instance.SetDataSwipe(currentPage - 1, currentPage);

        }
        StartCoroutine(SmoothMove(transform.position, newLocation, easing));
        panelLocation = newLocation;
    }
    public void PreviousPage()
    {
        Vector3 newLocation = panelLocation;
        if (currentPage > 1)
        {
            currentPage--;
            newLocation += new Vector3(Screen.width, 0, 0);

            GameManager.Instance.SetDataSwipe(currentPage + 1, currentPage);

        }
        StartCoroutine(SmoothMove(transform.position, newLocation, easing));
        panelLocation = newLocation;
    }

    public void GoToPanel(int pageNumber)
    {
        Vector3 newLocation = panelLocation;
        float factor = Screen.width;
        if (pageNumber > currentPage)
        {
            //GameManager.Instance.SetDataSwipe(currentPage, pageNumber);


            factor = (pageNumber - currentPage);
            currentPage = pageNumber;
            newLocation += new Vector3(-Screen.width*factor, 0,0);
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;
        }
        else if (pageNumber < currentPage)
        {
            //GameManager.Instance.SetDataSwipe(currentPage, pageNumber);


            factor = (currentPage - pageNumber);
            currentPage = pageNumber;
            newLocation += new Vector3(Screen.width*factor, 0,0);
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;    
        }
        else
        {
        }
        //factor *= (currentPage - pageNumber);
            //StartCoroutine(SmoothMove(transform.localPosition, panelLocation, easing));
    }

    public void OnDrag(PointerEventData data)
    {
        float difference = data.pressPosition.x - data.position.x;
        //Debug.Log(difference);
        transform.position = panelLocation - new Vector3(difference, 0, 0);
    }

    public void OnEndDrag(PointerEventData data)
    {
        float percentage = (data.pressPosition.x - data.position.x) / Screen.width;
        if (Mathf.Abs(percentage) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            if (percentage > 0 && currentPage < totalPages)
            {
                currentPage++;
                newLocation += new Vector3(-Screen.width, 0, 0);

                GameManager.Instance.SetDataSwipe(currentPage -1, currentPage);
            }
            else if (percentage < 0 && currentPage > 1)
            {
                currentPage--;
                newLocation += new Vector3(Screen.width, 0, 0);

                GameManager.Instance.SetDataSwipe(currentPage + 1, currentPage);
            }
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;
        }
        else
        {
            StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        }
    }

    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }
}
