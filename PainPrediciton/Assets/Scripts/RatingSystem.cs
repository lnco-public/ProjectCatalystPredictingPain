using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is containing methods to copy the question on top of the chart in rating system 
/// </summary>
public class RatingSystem : MonoBehaviour
{
    public List<Slider> sliderList;
    public Text questionDisplayed;

    private string _category;
    private string _question;

    public void SetCategory(Text c) { { _category = c.text; } }
    public void SetQuestion(Text q) { { _question = q.text;
            questionDisplayed.text = _question;
        } }

    void OnEnable()
    {
        for (int i = 0; i < sliderList.Count; i++)
        {
            sliderList[i].value = 0;
        }

        
    }

    private void OnDisable()
    {
        string answers = "";
        for (int i = 0; i < sliderList.Count; i++)
        {
            answers += "," + sliderList[i].value;
        }
        Debug.Log(_category + "," + _question + answers);
    }
}
