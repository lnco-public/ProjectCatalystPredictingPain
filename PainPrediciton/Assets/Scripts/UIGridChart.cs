using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This Method enable the creation and edition of a grid chart in a UI component
/// </summary>
public class UIGridChart : Graphic
{
    public Color axisColor;
    public Vector2Int gridSize = new Vector2Int(1, 1);
    public float thickness = 10f;

    [HideInInspector]
    public Vector2 origin, chartMax;

    float width, height, cellWidth, cellHeight;

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        width = rectTransform.rect.width;
        height = rectTransform.rect.height;

        Vector3[] worldCorners = new Vector3[4];
        rectTransform.GetWorldCorners(worldCorners);
        origin = Camera.main.WorldToScreenPoint(worldCorners[0]);
        chartMax = Camera.main.WorldToScreenPoint(worldCorners[2]);

        cellWidth = width / gridSize.x;
        cellHeight = height / gridSize.y;

        int count = 0;

        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                DrawCell(x, y, count, vh);
                count++;
            }
        }
        DrawAxis((count - 1) * 8 + 7, vh);

        //Debug.Log(origin+ " " + chartMax);
    }

    private void DrawAxis( int index, VertexHelper vh)
    {
        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = axisColor;

        float widthSqr = thickness * thickness;
        float distanceSqr = widthSqr / 2f;
        float distance = Mathf.Sqrt(distanceSqr);

        //1
        vertex.position = new Vector3(0, 0);
        vh.AddVert(vertex);
        //2
        vertex.position = new Vector3(0, height);
        vh.AddVert(vertex);
        //3
        vertex.position = new Vector3(distance*2, height);
        vh.AddVert(vertex);
        //4
        vertex.position = new Vector3(distance*2, distance*2);
        vh.AddVert(vertex);
        //5
        vertex.position = new Vector3(width, 0);
        vh.AddVert(vertex);
        //6
        vertex.position = new Vector3(width, distance*2);
        vh.AddVert(vertex);

        vh.AddTriangle(index + 1, index + 2, index + 3);
        vh.AddTriangle(index + 1, index + 3, index + 4);
        vh.AddTriangle(index + 1, index + 4, index + 6);
        vh.AddTriangle(index + 1, index + 5, index + 6);
    }

    private void DrawCell(int x, int y, int index, VertexHelper vh)
    {
        float xPos = cellWidth * x;
        float yPos = cellHeight * y;

        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        vertex.position = new Vector3(xPos, yPos);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos, yPos + cellHeight);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + cellWidth, yPos + cellHeight);
        vh.AddVert(vertex);

        vertex.position = new Vector3(xPos + cellWidth, yPos);
        vh.AddVert(vertex);

        //vh.AddTriangle(0, 1, 2);
        //vh.AddTriangle(2, 3, 0);

        float widthSqr = thickness * thickness;
        float distanceSqr = widthSqr / 2f;
        float distance = Mathf.Sqrt(distanceSqr);

        if (xPos == 0 && yPos == 0)
        {

            vertex.position = new Vector3(distance * 2, distance*2);
            vh.AddVert(vertex);

            vertex.position = new Vector3(distance * 2, (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3((cellWidth - distance), (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3((cellWidth - distance), distance*2);
            vh.AddVert(vertex);
        }
        else if (xPos == 0 && yPos != 0)
        {

            vertex.position = new Vector3(distance * 2, yPos + distance);
            vh.AddVert(vertex);

            vertex.position = new Vector3(distance * 2, yPos + (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3((cellWidth - distance), yPos + (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3((cellWidth - distance), yPos + distance);
            vh.AddVert(vertex);
        }
        else if (xPos != 0 && yPos == 0)
        {

            vertex.position = new Vector3(xPos + distance, distance * 2);
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + distance, (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + (cellWidth - distance), (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + (cellWidth - distance), distance * 2);
            vh.AddVert(vertex);
        }
        else
        {
            vertex.position = new Vector3(xPos + distance, yPos + distance);
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + distance, yPos + (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + (cellWidth - distance), yPos + (cellHeight - distance));
            vh.AddVert(vertex);

            vertex.position = new Vector3(xPos + (cellWidth - distance), yPos + distance);
            vh.AddVert(vertex);

        }


        int offSet = index * 8;

        //left edge
        vh.AddTriangle(offSet + 0, offSet + 1, offSet + 5);
        vh.AddTriangle(offSet + 5, offSet + 4, offSet + 0);

        //top edge 
        vh.AddTriangle(offSet + 1, offSet + 2, offSet + 6);
        vh.AddTriangle(offSet + 6, offSet + 5, offSet + 1);

        //right edge
        vh.AddTriangle(offSet + 2, offSet + 3, offSet + 7);
        vh.AddTriangle(offSet + 7, offSet + 6, offSet + 2);

        //bottom edge 
        vh.AddTriangle(offSet + 3, offSet + 0, offSet + 4);
        vh.AddTriangle(offSet + 4, offSet + 7, offSet + 3);

    }
}
